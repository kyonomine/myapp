# frozen_string_literal: true

Rails.application.routes.draw do
  root 'pages#index'
  get 'pages/index'
  get 'pages/show'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  resources :users do
    member do
      get 'product'
    end
    collection do
      get 'search'
    end
  end
  resources :products do
    collection do
      get 'search'
    end
    resources :likes, only: %i[index create destroy]
  end

  resources :messages, only: [:create]

  resources :rooms, only: %i[create show index]

  resources :diaries

  scope '(:locale)' do
    resources :users, except: :create do
      member do
        get :followings, :followers
      end
    end
    resource :user_icons, only: :destroy
    resources :relationships, only: %I[create destroy]
  end
end
