# frozen_string_literal: true

class DiariesController < ApplicationController
  def index
    @diaries = Diary.all
  end

  def new
    @diary = Diary.new
    @diary.diary_details.build
  end

  def show
    @diary = Diary.find(params[:id])
    @diary_details = @diary.diary_details
    @total_price = @diary_details.sum(:diary_product_price)
  end

  def create
    @diary = Diary.new(diary_params)
    @diary.user_id = current_user.id
    if @diary.save
      redirect_to diary_path(@diary)
    else
      render 'new'
    end
  end

  def destroy
    @diary = Diary.find(params[:id])
    @diary.destroy
    redirect_to diaries_path, notice: '削除しました'
  end

  def edit
    @diary = Diary.find(params[:id])
  end

  def update
    @diary = Diary.find(params[:id])
    if @diary.update(diary_params)
      redirect_to diary_path(@diary), notice: '編集しました'
    else
      render 'edit'
    end
  end

  private

  def diary_params
    params.require(:diary).permit(
      :title,
      :content,
      :start_time,
      :product_name,
      :product_alcohol,
      :product_price,
      :impression,
      :id,
      diary_details_attributes: %i[
        id
        diary_product_name
        diary_product_alcohol
        diary_product_price
        impression
        total
        _destroy
      ]
    ).merge(user_id: current_user.id)
  end
end
