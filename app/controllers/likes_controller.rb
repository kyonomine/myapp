# frozen_string_literal: true

class LikesController < ApplicationController
  def index
    user_id = current_user.id
    @user = User.find(user_id)
    @like = Like.find_by(product_id: params[:product_id], user_id: current_user.id)
  end

  def create
    @like = current_user.likes.create(product_id: params[:product_id])
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @like = Like.find_by(product_id: params[:product_id], user_id: current_user.id)
    @like.destroy
    redirect_back(fallback_location: root_path)
  end
end
