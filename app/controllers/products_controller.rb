# frozen_string_literal: true

class ProductsController < ApplicationController
  def index
    @products  = Product.all
  end

  def new
    @product = Product.new
  end

  def show
    @product = Product.find(params[:id])
    @user = User.find_by(id: @product.user_id)
    @like = Like.new
  end

  def create
    @product = Product.new(product_params)
    @product.user_id = current_user.id
    if @product.save
      flash[:success] = '商品を投稿しました'
      redirect_to product_user_path(@product.user_id)
    else
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
      redirect_to product_path
    else
      render :edit
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:success] = 'product deleted'
    redirect_to product_user_path(@product.user_id)
  end

  def search
    @product = Product.search(params[:search])
  end

  private

  def product_params
    params.require(:product).permit(:product_name, :product_price, :product_introduction, :product_alcohol,
                                    :product_company, :id, :product_image, :appeal, :evaluation)
  end

  def ensure_current_user
    product = Product.find(params[:id])
    redirect_to action: :index if product.user_id != current_user.id
  end

  def set_product
    @product = Product.find(params[:id])
  end
end
