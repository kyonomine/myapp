# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :username, presence: true
  validates :password, presence: true, length: { minimum: 6 },
                       format: { with: /\A[a-z0-9]+\z/i, message: '英数字以外の文字を含めることは出来ません' }
  validates :email, presence: true, uniqueness: true,
                    format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i, message: '英数字以外の文字を含めることは出来ません' }

  mount_uploader :image, ImageUploader

  has_many :followers,
           class_name: 'Relationship',
           foreign_key: 'follower_id',
           dependent: :destroy,
           inverse_of: :follower
  has_many :followings,
           class_name: 'Relationship',
           foreign_key: 'following_id',
           dependent: :destroy,
           inverse_of: :following

  has_many :following_users, through: :followers, source: :following
  has_many :follower_users, through: :followings, source: :follower

  has_many :products
  has_many :likes, dependent: :destroy
  has_many :liked_products, through: :likes, source: :product

  has_many :messages, dependent: :destroy
  has_many :entries, dependent: :destroy
  has_many :diaries

  def already_liked?(product)
    likes.exists?(product_id: product.id)
  end

  def follow(other_user_id)
    followers.create(following_id: other_user_id)
  end

  def following?(other_user)
    following_users.include?(other_user)
  end

  def self.search(search)
    if search
      User.where(['username LIKE(?) OR profile LIKE(?)', "%#{search}%", "%#{search}%"])
    else
      User.all
    end
  end
end
