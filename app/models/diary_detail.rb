# frozen_string_literal: true

class DiaryDetail < ApplicationRecord
  belongs_to :diary

  validates :diary_product_name, presence: true
  validates :diary_product_price, presence: true
  validates :diary_product_alcohol, presence: true
end
