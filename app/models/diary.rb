# frozen_string_literal: true

class Diary < ApplicationRecord
  belongs_to :user
  has_many :diary_details, dependent: :destroy
  accepts_nested_attributes_for :diary_details, allow_destroy: true

  validates :title, presence: true
end
