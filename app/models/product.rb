# frozen_string_literal: true

class Product < ApplicationRecord
  belongs_to :user
  has_many :likes
  has_many :liked_users, through: :likes, source: :user

  validates :product_name, presence: true
  validates :product_price, presence: true
  validates :product_alcohol, presence: true

  mount_uploader :product_image, ImageUploader

  def self.search(search)
    if search
      Product.where([
                      'product_name LIKE(?) OR product_price LIKE(?) OR product_introduction LIKE(?) OR product_alcohol LIKE(?) OR product_company LIKE(?)', "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%"
                    ])
    else
      Product.all
    end
  end
end
