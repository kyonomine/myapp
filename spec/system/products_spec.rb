require 'rails_helper'

RSpec.describe 'Products', type: :system do
  let(:user_a) { create(:user, username: 'test1', email: 'test_1@google.com', password: 'password01') }
  let(:user_b) { create(:user, username: 'test2', email: 'test_2@google.com', password: 'password02') }
  let!(:product_a) do
    create(:product, product_name: 'クラフトビール', product_price: '100', product_alcohol: '5', user: user_a)
  end
  let!(:product_b) { create(:product, product_name: 'ワイン', product_price: '200', product_alcohol: '10', user: user_b) }

  before do
    visit new_user_session_path
    fill_in 'user[email]', with: login_user.email
    fill_in 'user[password]', with: login_user.password
    click_button 'ログイン'
  end

  describe '新規投稿' do
    let(:login_user) { user_a }
    before do
      visit new_product_path(user_a)
    end
    context '入力値が正常のとき' do
      it '新規投稿が成功' do
        fill_in 'product[product_name]', with: 'sake'
        fill_in 'product[appeal]', with: '一番好き'
        fill_in 'product[product_price]', with: '100'
        fill_in 'product[product_introduction]', with: '美味しい'
        fill_in 'product[product_alcohol]', with: '5'
        fill_in 'product[product_company]', with: 'test_company'
        click_button '登録'
        expect(current_path).to eq product_user_path(user_a)
      end
    end
    context '入力値が異常のとき' do
      before do
        fill_in 'product[product_name]', with: ''
        fill_in 'product[appeal]', with: ''
        fill_in 'product[product_price]', with: ''
        fill_in 'product[product_introduction]', with: ''
        fill_in 'product[product_alcohol]', with: ''
        fill_in 'product[product_company]', with: ''
        click_button '登録'
      end
      it '投稿がエラーになる' do
        within '#error_explanation' do
          expect(page).to have_content '商品名を入力してください'
          expect(page).to have_content '価格を入力してください'
          expect(page).to have_content 'アルコール度数を入力してください'
        end
      end
    end
  end
  describe '投稿詳細' do
    context 'user_aでログインしているとき' do
      let(:login_user) { user_a }
      before do
        visit product_path(product_a)
      end
      it 'user_aが作成した投稿詳細の表示に成功' do
        expect(page).to have_content 'クラフトビール'
        expect(page).to have_content '100'
        expect(page).to have_content '5'
      end
    end
    context 'user_bでログインしているとき' do
      let(:login_user) { user_b }
      before do
        visit product_path(product_b)
      end
      it 'user_aが作成した投稿詳細が表示されないこと' do
        expect(page).not_to have_content 'クラフトビール'
        expect(page).not_to have_content '100'
        expect(page).not_to have_content '5'
      end
    end
  end
  describe '投稿編集' do
    let(:login_user) { user_a }
    before do
      visit edit_product_path(product_a)
    end
    context '正常の値が入力された場合' do
      it '投稿編集が成功' do
        fill_in 'product[product_name]', with: '酒'
        fill_in 'product[appeal]', with: '一番美味しい'
        fill_in 'product[product_price]', with: '500'
        fill_in 'product[product_introduction]', with: 'うまい'
        fill_in 'product[product_alcohol]', with: '7'
        fill_in 'product[product_company]', with: 'sumple_company'
        click_button '更新'
        expect(current_path).to eq product_path(product_a)
        expect(page).to have_content '酒'
        expect(page).to have_content '一番美味しい'
        expect(page).to have_content '500'
        expect(page).to have_content 'うまい'
        expect(page).to have_content '7'
        expect(page).to have_content 'sumple_company'
      end
    end
    context '商品名、価格、アルコール度数が未入力の場合' do
      it '商品編集が失敗' do
        fill_in 'product[product_name]', with: nil
        fill_in 'product[appeal]', with: '一番美味しい'
        fill_in 'product[product_price]', with: nil
        fill_in 'product[product_introduction]', with: 'うまい'
        fill_in 'product[product_alcohol]', with: nil
        fill_in 'product[product_company]', with: 'sumple_company'
        click_button '更新'
        expect(page).to have_content '商品名を入力してください'
        expect(page).to have_content '価格を入力してください'
        expect(page).to have_content 'アルコール度数を入力してください'
      end
    end
  end
  describe '投稿削除' do
    let(:login_user) { user_a }
    before do
      visit product_path(product_a)
    end
    it '商品投稿の削除が成功' do
      click_link '削除する'
      expect(page).not_to have_content 'クラフトビール'
    end
  end

  describe 'search' do
    let(:login_user) { user_b }
    before do
      visit search_products_path(user_b)
    end
    context '完全一致' do
      it 'showに表示されているコンテンツ' do
        fill_in 'search', with: 'クラフトビール'
        click_button 'Search'
        expect(page).to have_content 'クラフトビール'
        expect(page).not_to have_content 'ワイン'
      end
    end
    context '部分一致' do
      it 'showに表示されているコンテンツ' do
        fill_in 'search', with: 'ビール'
        click_button 'Search'
        expect(page).to have_content 'クラフトビール'
      end
    end
  end
end
