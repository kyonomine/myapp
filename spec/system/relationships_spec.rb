# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Relationships', type: :system do
  let(:user) { FactoryBot.create(:user) }
  let(:other_users) { FactoryBot.create_list(:user, 20) }

  before do
    visit new_user_session_path
    fill_in 'user[email]', with: login_user.email
    fill_in 'user[password]', with: login_user.password
    click_button 'ログイン'
  end

  describe 'show' do
    let(:login_user) { user }
    before do
      other_users[0..9].each do |other_user|
        user.followers.create!(following_id: other_user.id)
        user.followings.create!(follower_id: other_user.id)
      end
    end
    context 'フォロー機能' do
      before do
        visit user_path(other_users.first.id)
      end
      it 'フォローを解除する' do
        find('.no_follow_btn').click
        expect(page).not_to have_content 'フォローする'
      end
    end
    context 'フォロー機能' do
      before do
        visit user_path(other_users.last.id)
      end
      it 'フォローする' do
        find('.yes_follow_btn').click
        expect(page).not_to have_content 'フォローを解除する'
      end
    end
  end
end
