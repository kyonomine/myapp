# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Diaries', type: :system do
  let(:user_a) { create(:user, username: 'test_a', email: 'test_a@example.com', password: 'password') }

  before do
    visit new_user_session_path
    fill_in 'user[email]', with: login_user.email
    fill_in 'user[password]', with: login_user.password
    click_button 'ログイン'
  end

  describe '#new' do
    let(:login_user) { user_a }
    it '新規の日記投稿が成功' do
      visit new_diary_path(user_a)
      fill_in 'diary[title]', with: '宅飲み'
      fill_in 'diary[diary_details_attributes][0][diary_product_name]', with: 'アサヒ'
      fill_in 'diary[diary_details_attributes][0][diary_product_price]', with: '200'
      fill_in 'diary[diary_details_attributes][0][diary_product_alcohol]', with: '7'
      fill_in 'diary[impression]', with: '美味しかった'
      click_button '登録'
      expect(page).to have_content '宅飲み'
      expect(page).to have_content 'アサヒ'
      expect(page).to have_content '200'
      expect(page).to have_content '7'
      expect(page).to have_content '美味しかった'
    end
  end

  describe '#edit' do
    let(:login_user) { user_a }
    let(:diary_detail_params) { attributes_for(:diary_detail) }
    let(:diary_a) { create(:diary, diary_details_attributes: [diary_detail_params]) }

    it '日記投稿の編集が成功' do
      visit edit_diary_path(diary_a)
      click_on '追加'
      fill_in 'diary[title]', with: '飲み会'
      fill_in 'diary[diary_details_attributes][0][diary_product_name]', with: '生ビール'
      fill_in 'diary[diary_details_attributes][0][diary_product_price]', with: '300'
      fill_in 'diary[diary_details_attributes][0][diary_product_alcohol]', with: '5'
      fill_in 'diary[impression]', with: '自分へのご褒美'
      click_button '編集完了'
      expect(current_path).to eq diary_path(diary_a)
      expect(page).to have_content '飲み会'
      expect(page).to have_content '生ビール'
      expect(page).to have_content '300'
      expect(page).to have_content '5'
      expect(page).to have_content '自分へのご褒美'
      expect(page).to have_content '編集しました'
    end
  end

  describe '#destroy' do
    let(:login_user) { user_a }
    let(:diary_detail_params) { attributes_for(:diary_detail) }
    let(:diary_a) { create(:diary, diary_details_attributes: [diary_detail_params]) }
    it '商品投稿の削除が成功' do
      visit edit_diary_path(diary_a)
      click_on '日記削除'
      expect(page).not_to have_content '宅飲み'
    end
  end
end
