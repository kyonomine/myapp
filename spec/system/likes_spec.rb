# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Likes', type: :system do
  let(:user_a) { create(:user, username: 'test_a', email: 'test_a@example.com', password: 'password') }
  let!(:product) { create(:product, product_name: 'クラフトビール', product_price: '100', product_alcohol: '5', user: user_a) }

  before do
    visit new_user_session_path
    fill_in 'user[email]', with: login_user.email
    fill_in 'user[password]', with: login_user.password
    click_button 'ログイン'
  end

  describe 'いいね機能の登録と解除' do
    let(:login_user) { user_a }
    context 'いいねする' do
      before do
        visit product_path(product)
      end
      it 'いいねできる事' do
        find('.unlikes').click
        expect(page).to have_selector '#liking-btn'
      end
    end
    context 'いいねを解除' do
      let!(:like) { create(:like, product_id: product.id, user_id: user_a.id) }
      before do
        visit product_path(product)
      end
      it 'いいねを解除できる事' do
        find('.likes').click
        expect(page).to have_selector '#nolike-btn'
      end
    end
  end
end
