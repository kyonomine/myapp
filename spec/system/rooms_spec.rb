# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Rooms', type: :system do
  let(:user_a) do
    create(:user, username: 'test1', email: 'test_1@google.com', password: 'password01', profile: 'はじめまして')
  end
  let(:user_b) do
    create(:user, username: 'test2', email: 'test_2@google.com', password: 'password02', profile: 'お酒好きです')
  end
  let(:room) { create(:room) }
  let(:entry) { create(:entry) }
  let(:message) { create(:message) }

  before do
    visit new_user_session_path
    fill_in 'user[email]', with: login_user.email
    fill_in 'user[password]', with: login_user.password
    click_button 'ログイン'
  end
  describe 'メッセージ投稿' do
    let(:login_user) { user_a }
    before do
      visit user_path(user_b)
      click_on 'チャット'
    end
    it 'チャットの履歴がないとき' do
      expect(page).to have_content 'メッセージはまだありません'
    end
    it '投稿したメッセージを表示する' do
      fill_in 'message[content]', with: 'こんにちは'
      click_on '送信'
      expect(page).to have_content 'こんにちは'
    end
  end
end
