# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :system do
  let!(:user_a) do
    create(:user, username: 'test_a', email: 'test_a@example.com', password: 'password', profile: 'よろしくお願いします')
  end
  let!(:sumple_a) do
    create(:user, username: 'sumple_a', email: 'sumple_a@example.com', password: 'password', profile: 'よろしく')
  end
  before do
    visit new_user_session_path
    fill_in 'user[email]', with: login_user.email
    fill_in 'user[password]', with: login_user.password
    click_button 'ログイン'
  end
  describe 'show' do
    let(:login_user) { user_a }
    describe 'ユーザー詳細(ログインユーザー)' do
      before do
        visit user_path(user_a)
      end
      it 'showに表示されているコンテンツ' do
        expect(page).to have_content login_user.username
        expect(page).to have_content login_user.profile
      end
      it 'フォロー' do
        click_link 'フォロー'
        expect(current_path).to eq followings_user_path(user_a)
      end
      it 'フォロワー' do
        click_link 'フォロワー'
        expect(current_path).to eq followers_user_path(user_a)
      end
      it '投稿一覧' do
        click_link '投稿一覧'
        expect(current_path).to eq product_user_path(user_a)
      end
      it '編集する' do
        click_on 'アカウント編集'
        expect(current_path).to eq edit_user_registration_path
      end
      it '商品登録' do
        click_on '商品登録'
        expect(current_path).to eq new_product_path
      end
      it 'いいねした投稿' do
        click_on 'いいねした投稿'
        expect(current_path).to eq product_likes_path(user_a)
      end
      it '日記をかく' do
        click_on '日記をかく'
        expect(current_path).to eq diaries_path
      end
    end
  end

  describe 'search' do
    let(:login_user) { user_a }
    before do
      visit search_users_path(user_a)
    end
    context '完全一致' do
      it 'showに表示されているコンテンツ' do
        fill_in 'search', with: 'sumple'
        click_button 'Search'
        expect(page).to have_content 'sumple'
        expect(page).not_to have_content 'example'
      end
    end
    context '部分一致' do
      it 'showに表示されているコンテンツ' do
        fill_in 'search', with: 'sumple'
        click_button 'Search'
        expect(page).to have_content 'sumple_a'
      end
    end
  end
end
