# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DeivseUsers', type: :system do
  let(:user_a) { create(:user, username: 'test_a', email: 'test_a@example.com', password: 'password') }
  let(:test_user) { create(:user, username: 'test_user', email: 'xx@example.com', password: 'password') }
  describe 'アカウント作成機能' do
    it 'ユーザーの新規作成が成功' do
      expect do
        visit new_user_registration_path
        fill_in 'user[username]', with: 'sumple'
        fill_in 'user[email]', with: 'sumple@example.com'
        fill_in 'user[password]', with: 'password'
        fill_in 'user[password_confirmation]', with: 'password'
        click_button 'アカウント作成'
        expect(page).to have_content 'アカウント登録が完了しました。'
      end.to change { User.count }.by(1)
    end
  end

  describe 'ログイン機能' do
    before do
      visit new_user_session_path
    end
    it '「ログイン」をクリックするとログインできる' do
      @user = User.create!(username: 'test1', email: 'test10@example.com', password: 'password')
      fill_in 'user[email]', with: 'test10@example.com'
      fill_in 'user[password]', with: 'password'
      click_button 'ログイン'
      expect(current_path).to eq user_path(@user)
      expect(page).to have_content 'ログインしました。'
      expect(page).to have_content 'test1'
    end
    it '「テストアカウントでログイン」をクリックするとテストアカウントでログインできる' do
      @user = User.create!(username: 'test_user', email: 'xx@example.com', password: 'password')
      click_button 'テストアカウントでログイン'
      expect(current_path).to eq user_path(@user)
      expect(page).to have_content 'ログインしました。'
      expect(page).to have_content 'test_user'
    end
    it '「アカウント作成」をクリックするとサインアップに遷移する' do
      click_link 'アカウント作成'
      expect(current_path).to eq new_user_registration_path
    end
  end

  describe 'ログアウト機能' do
    before do
      visit new_user_session_path
      fill_in 'user[email]', with: login_user.email
      fill_in 'user[password]', with: login_user.password
      click_button 'ログイン'
    end
    describe 'ログイン後' do
      let(:login_user) { user_a }
      describe 'ユーザー削除' do
        it 'ユーザーの削除が成功' do
          visit root_path(user_a)
          click_link 'ログアウト'
          expect(page).to have_current_path(root_path)
          expect(page).to have_content 'サインアップ'
          expect(page).to have_content 'ログイン'
        end
      end
    end
  end

  describe 'edit' do
    before do
      visit new_user_session_path
      fill_in 'user[email]', with: login_user.email
      fill_in 'user[password]', with: login_user.password
      click_button 'ログイン'
    end
    describe 'ログイン後' do
      let(:login_user) { user_a }
      describe 'ユーザー編集' do
        context 'フォームの入力値が正常' do
          it 'ユーザーの編集が成功' do
            visit edit_user_registration_path(user_a)
            fill_in 'user[username]', with: 'sumple_user'
            fill_in 'user[email]', with: 'sumple@example.com'
            fill_in 'user[profile]', with: 'よろしくお願いします'
            fill_in 'user[password]', with: 'sumple'
            fill_in 'user[password_confirmation]', with: 'sumple'
            fill_in 'user[current_password]', with: 'password'
            click_button '更新'
            expect(current_path).to eq user_path(user_a)
            expect(page).to have_content 'アカウント情報を変更しました。'
          end
        end
        context 'メールアドレス未記入' do
          it 'ユーザーの編集が失敗' do
            visit edit_user_registration_path(user_a)
            fill_in 'user[username]', with: 'sumple_user'
            fill_in 'user[email]', with: nil
            fill_in 'user[profile]', with: 'よろしくお願いします'
            fill_in 'user[password]', with: 'sumple'
            fill_in 'user[password_confirmation]', with: 'sumple'
            fill_in 'user[current_password]', with: 'password'
            click_button '更新'
            expect(page).to have_content 'メールアドレスは有効でありません。'
          end
        end
      end
    end
  end

  describe 'destroy' do
    before do
      visit new_user_session_path
      fill_in 'user[email]', with: login_user.email
      fill_in 'user[password]', with: login_user.password
      click_button 'ログイン'
    end
    describe 'ログイン後' do
      let(:login_user) { user_a }
      describe 'ユーザー削除' do
        it 'ユーザーの削除が成功' do
          expect do
            visit edit_user_registration_path(user_a)
            click_on 'アカウントを削除する'
            expect(page).to have_current_path(root_path)
            expect(page).to have_content 'アカウントを削除しました。'
          end.to change { User.count }.by(-1)
        end
      end
    end
  end
end
