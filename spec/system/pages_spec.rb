# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Pages', type: :system do
  let(:user_a) { create(:user, username: 'test1', email: 'test_1@google.com', password: 'password01') }
  before do
    visit root_path
  end
  context 'ログインしていない場合' do
    it 'ページに遷移できるか' do
      click_link '酒楽'
      expect(current_path).to eq pages_index_path
    end
    it 'ページに遷移できるか' do
      click_link 'サインアップ'
      expect(current_path).to eq new_user_registration_path
    end
    it 'ページに遷移できるか' do
      click_link 'ログイン'
      expect(current_path).to eq new_user_session_path
    end
    it 'ページに遷移できるか' do
      click_link '好きなお酒を見つける'
      expect(current_path).to eq new_user_session_path
    end
    it 'ページに遷移できるか' do
      click_link '飲んだお酒を共有する'
      expect(current_path).to eq new_user_session_path
    end
    it 'ページに遷移できるか' do
      click_link '飲み友を見つける'
      expect(current_path).to eq new_user_session_path
    end
    it 'ページに遷移できるか' do
      click_link '飲んだお酒を管理する'
      expect(current_path).to eq new_user_session_path
    end
  end
  context 'ログインしている場合' do
    before do
      visit new_user_session_path
      fill_in 'user[email]', with: login_user.email
      fill_in 'user[password]', with: login_user.password
      click_button 'ログイン'
      visit root_path
    end
    let(:login_user) { user_a }
    it 'ページに遷移できるか' do
      click_link '酒楽'
      expect(current_path).to eq pages_index_path
    end
    it 'トップページにusermnameが表示できているか' do
      expect(page).to have_content 'test1'
    end
    it 'ページに遷移できるか' do
      click_link '好きなお酒を見つける'
      expect(current_path).to eq search_products_path
    end
    it 'ページに遷移できるか' do
      click_link '飲んだお酒を共有する'
      expect(current_path).to eq new_product_path
    end
    it 'ページに遷移できるか' do
      click_link '飲み友を見つける'
      expect(current_path).to eq search_users_path
    end
    it 'ページに遷移できるか' do
      click_link '飲んだお酒を管理する'
      expect(current_path).to eq diaries_path
    end
  end
end
