# frozen_string_literal: true

FactoryBot.define do
  factory :diary do
    user
    title { '宅飲み' }
    impression { '一番おいしい' }
    start_time { '2021/01/01' }
  end
end
