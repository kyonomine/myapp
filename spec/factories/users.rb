# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    username { 'user1' }
    email { Faker::Internet.email }
    password { 'password01' }
    profile { 'よろしくお願いします' }
    image { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
  end
end
