# frozen_string_literal: true

FactoryBot.define do
  factory :diary_detail do
    diary_product_name { 'クラフトビール' }
    diary_product_price { '100' }
    diary_product_alcohol { '1' }
  end
end
