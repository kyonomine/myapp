# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    user_id { FactoryBot.create(:user).id }
    room_id { FactoryBot.create(:room).id }
  end
end
