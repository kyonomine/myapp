# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    product_name { 'ビール' }
    product_price { Faker::Number.between(from: 100, to: 10_000) }
    product_alcohol { Faker::Number.between(from: 1, to: 100) }
    product_company { 'test_company' }
    product_introduction { 'test_introduction' }
    appeal { 'test_appeal' }
    evaluation { Faker::Number.between(from: 1, to: 5) }
    product_image { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
    user
  end
end
