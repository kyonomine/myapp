# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product, type: :model do
  let!(:user) { create(:user) }
  let!(:product) { create(:product) }

  describe 'バリデーションのテスト' do
    it 'product_name、product_price、product_alcoholがあれば有効な状態であること' do
      expect(product).to be_valid
    end
    it 'product_nameが無ければ無効であること' do
      product.product_name = ''
      product.valid?
      expect(product.errors[:product_name]).to include('を入力してください')
    end
    it 'product_priceが無ければ無効であること' do
      product.product_price = ''
      product.valid?
      expect(product.errors[:product_price]).to include('を入力してください')
    end
    it 'product_alcoholが無ければ無効であること' do
      product.product_alcohol = ''
      product.valid?
      expect(product.errors[:product_alcohol]).to include('を入力してください')
    end
    it 'product_imageがnilの場合でも保存できること' do
      product = build(:product, product_image: nil)
      expect(product).to be_valid
    end
    it 'product_companyがnilの場合でも保存できること' do
      product = build(:product, product_company: nil)
      expect(product).to be_valid
    end
    it 'product_introductionがnilの場合でも保存できること' do
      product = build(:product, product_introduction: nil)
      expect(product).to be_valid
    end
    it 'appealがnilの場合でも保存できること' do
      product = build(:product, appeal: nil)
      expect(product).to be_valid
    end
    it 'evaluationがnilの場合でも保存できること' do
      product = build(:product, evaluation: nil)
      expect(product).to be_valid
    end
  end

  describe '#search' do
    context '一致するデータが存在する場合' do
      it '検索文字列に完全一致する配列を返すこと' do
        expect(Product.search('ビール')).to include(product)
      end
      it '検索文字列に部分一致する配列を返すこと' do
        expect(Product.search('ビー')).to include(product)
      end
    end
    context '一致するデータが存在しない場合' do
      it '検索文字列が一致しない場合、空の配列を返すこと' do
        expect(Product.search('product2')).to be_empty
      end
      it '検索文字列が空白の場合、全ての配列を返すこと' do
        expect(Product.search('')).to include(product)
      end
    end
  end

  describe '各モデルとのアソシエーション' do
    let(:association) do
      described_class.reflect_on_association(target)
    end
    let(:product) { create(:product) }
    let(:target) { :likes }
    it 'Likeとの関連付けはhas_manyであること' do
      expect(association.macro).to eq :has_many
    end
  end
end
