# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Diary, type: :model do
  before do
    @user = FactoryBot.build(:user)
    @diary = FactoryBot.build(:diary)
  end

  describe 'バリデーションのテスト' do
    it 'titleがあれば有効な状態であること' do
      expect(@diary).to be_valid
    end

    it 'titleが無ければ無効であること' do
      @diary.title = ''
      @diary.valid?
      expect(@diary.errors[:title]).to include('を入力してください')
    end
  end
end
