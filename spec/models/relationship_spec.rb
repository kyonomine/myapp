# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Relationship, type: :model do
  let(:relationship) { create(:relationship) }
  describe 'バリデーションのテスト' do
    it 'follower_id、following_idがあれば有効な状態であること' do
      expect(relationship).to be_valid
    end
    it 'follower_idが無ければ無効であること' do
      relationship.follower_id = nil
      relationship.valid?
      expect(relationship.errors[:follower_id]).to include('を入力してください')
    end
    it 'following_idが無ければ無効であること' do
      relationship.following_id = nil
      relationship.valid?
      expect(relationship.errors[:following_id]).to include('を入力してください')
    end
  end
  let(:association) do
    described_class.reflect_on_association(target)
  end
  describe '各モデルとのアソシエーション' do
    context 'followerとのアソシエーション' do
      let(:target) { :follower }
      it 'Followerとの関連付けはbelongs_toであること' do
        expect(association.macro).to  eq :belongs_to
      end
    end
    context 'followingとのアソシエーション' do
      let(:target) { :following }
      it 'Followingとの関連付けはbelongs_toであること' do
        expect(association.macro).to  eq :belongs_to
      end
    end
  end
end
