# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Entry, type: :model do
  let(:entry) { create(:entry) }

  describe '#create' do
    context '保存できる場合' do
      it '全てのパラメーターが揃っていれば保存できる' do
        expect(entry).to be_valid
      end
    end
    context '保存できない場合' do
      it 'user_idがnilの場合は保存できない' do
        entry.user_id = nil
        entry.valid?
        expect(entry.errors[:user]).to include('を入力してください')
      end
      it 'room_idがnilの場合は保存できない' do
        entry.room_id = nil
        entry.valid?
        expect(entry.errors[:room]).to include('を入力してください')
      end
    end
  end
end
