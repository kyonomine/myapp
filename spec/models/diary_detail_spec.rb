# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DiaryDetail, type: :model do
  before do
    @user = FactoryBot.build(:user)
    @diary = FactoryBot.build(:diary)
    @diary_detail = FactoryBot.build(:diary_detail)
  end

  describe 'バリデーションのテスト' do
    it 'diary_product_name、diary_product_price、diary_product_alcoholがあれば有効な状態であること' do
      expect(@diary).to be_valid
    end
    it 'diary_product_nameが無ければ無効であること' do
      @diary_detail.diary_product_name = ''
      @diary_detail.valid?
      expect(@diary_detail.errors[:diary_product_name]).to include('を入力してください')
    end
    it 'diary_product_priceが無ければ無効であること' do
      @diary_detail.diary_product_price = ''
      @diary_detail.valid?
      expect(@diary_detail.errors[:diary_product_price]).to include('を入力してください')
    end
    it 'diary_product_alcoholが無ければ無効であること' do
      @diary_detail.diary_product_alcohol = ''
      @diary_detail.valid?
      expect(@diary_detail.errors[:diary_product_alcohol]).to include('を入力してください')
    end
  end
end
