# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Like, type: :model do
  before do
    @user = FactoryBot.build(:user)
    @product = FactoryBot.build(:product)
    @like = FactoryBot.build(:like)
  end

  describe '正常値と異常値の確認' do
    it 'user_idとproduct_idがあれば保存できる' do
      expect(FactoryBot.create(:like)).to be_valid
    end
    it 'product_idが同じでもuser_idが違うと保存できる' do
      like = create(:like)
      expect(FactoryBot.create(:like, product_id: like.product_id)).to be_valid
    end
    it 'user_idが同じでもproduct_idが違うと保存できる' do
      like = create(:like)
      expect(FactoryBot.create(:like, user_id: like.user_id)).to be_valid
    end
  end

  describe '各モデルとのアソシエーション' do
    let(:association) do
      described_class.reflect_on_association(target)
    end
    context 'Userモデルとのアソシエーション' do
      let(:target) { :user }
      it 'Userとの関連付けはbelongs_toであること' do
        expect(association.macro).to  eq :belongs_to
      end
    end
    context 'Productモデルとのアソシエーション' do
      let(:target) { :product }
      it 'Productとの関連付けはbelongs_toであること' do
        expect(association.macro).to  eq :belongs_to
      end
    end
  end
end
