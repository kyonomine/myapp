# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user) { create(:user) }

  describe 'バリデーションのテスト' do
    it 'username、email、password、password_confirmationがあれば有効な状態であること' do
      expect(user).to be_valid
    end
    it 'usernameが無ければ無効であること' do
      user.username = ''
      user.valid?
      expect(user.errors[:username]).to include('を入力してください')
    end
    it 'emailが無ければ無効であること' do
      user.email = ''
      user.valid?
      expect(user.errors[:email]).to include('が入力されていません。')
    end
    it 'emailが重複していると無効であること' do
      another_user = build(:user, email: user.email)
      another_user.valid?
      expect(another_user.errors[:email]).to include('は既に使用されています。')
    end
    it 'passwordが無ければ無効であること' do
      user.password = ''
      user.valid?
      expect(user.errors[:password]).to include('が入力されていません。')
    end
    it 'passwordが存在してもpassword_confirmationがない場合は登録できないこと' do
      @user = build(:user, password_confirmation: '')
      @user.valid?
      expect(@user.errors[:password_confirmation]).to include('とパスワードの入力が一致しません')
    end
    it 'passwordが6文字以下だと無効であること' do
      user.password = 'aaaaa'
      user.valid?
      expect(user.errors[:password]).to include('は6文字以上に設定して下さい。')
    end
    it 'imageがnilの場合でも保存できること' do
      user.image = nil
      expect(user).to be_valid
    end
    it 'profileがnilの場合でも保存できること' do
      user.profile = nil
      expect(user).to be_valid
    end
  end

  describe '#search' do
    context '一致するデータが存在する場合' do
      it '検索文字列に完全一致する配列を返すこと' do
        expect(User.search('user1')).to include(user)
      end
      it '検索文字列に部分一致する配列を返すこと' do
        expect(User.search('user')).to include(user)
      end
    end
    context '一致するデータが存在しない場合' do
      it '検索文字列が一致しない場合、空の配列を返すこと' do
        expect(User.search('user2')).to be_empty
      end
      it '検索文字列が空白の場合、全ての配列を返すこと' do
        expect(User.search('')).to include(user)
      end
    end
  end
end
