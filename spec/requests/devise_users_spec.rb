# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User::request', type: :request do
  let!(:user) { create(:user) }
  let(:user_params) { attributes_for(:user) }
  let(:invalid_user_params) { attributes_for(:user, username: '') }
  let(:update_user_params) { attributes_for(:user, username: 'sumple') }

  describe 'GET #new' do
    before do
      get new_user_session_path
    end
    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end
  end

  describe 'GET #edit' do
    subject { get edit_user_registration_path }
    context 'ログインしている場合' do
      before do
        sign_in user
      end
      it 'リクエストが成功すること' do
        is_expected.to eq 200
      end
    end
  end

  describe 'POST #create' do
    context 'パラメータが妥当な場合' do
      it 'リクエストが成功すること' do
        post user_registration_path, params: { user: user_params }
        expect(response.status).to eq 302
      end
      it 'createが成功すること' do
        expect do
          post user_registration_path, params: { user: user_params }
        end
      end
    end
    context 'パラメータが不正な場合' do
      it 'リクエストが成功すること' do
        post user_registration_path, params: { user: invalid_user_params }
        expect(response.status).to eq 200
      end
      it 'createが失敗すること' do
        post user_registration_path, params: { user: invalid_user_params }
      end
      it 'エラーが表示されること' do
        post user_registration_path, params: { user: invalid_user_params }
        expect(response.body).to include 'のエラーが発生したため ユーザー は保存されませんでした。'
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'ユーザーを削除する' do
      sign_in user
      expect { delete user_registration_path, params: { user: user_params } }.to change(User, :count).by(-1)
    end
    it 'ユーザーを削除した後のリダイレクト' do
      sign_in user
      delete user_registration_path, params: { user: user_params }
      expect(response).to redirect_to root_url
    end
  end
end
