# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Likes', type: :request do
  let(:user) { create(:user, username: 'test_a', email: 'test_a@example.com', password: 'password') }
  let(:product) { create(:product, product_name: 'ビール', product_price: '100', product_alcohol: '5', user: user) }
  let(:product_a) { create(:product, product_name: 'ワイン', product_price: '200', product_alcohol: '10', user: user) }
  let!(:like) { create(:like, product_id: product.id, user_id: user.id) }

  before do
    sign_in user
  end

  describe 'GET #index' do
    it 'リクエストが成功すること' do
      get product_likes_path(product)
      expect(response.status).to eq 200
    end
    context 'いいねしているproductが表示されること' do
      before do
        get product_likes_path(product)
      end
      it 'productの商品名が表示されること' do
        expect(response.body).to include 'ビール'
      end
    end
    context 'いいねしていないproduct_aが表示されないこと' do
      before do
        get product_likes_path(product_a)
      end
      it 'product_aの商品名が表示されないこと' do
        expect(response.body).not_to include 'ワイン'
      end
    end
  end
end
