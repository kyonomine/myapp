# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User::request', type: :request do
  let(:user) { create(:user) }
  let(:product) { create(:product) }
  before do
    sign_in user
  end

  describe 'GET #show' do
    it 'リクエストが成功すること' do
      get users_path(user)
      expect(response.status).to eq 200
    end
    it 'showアクションにユーザー名が存在する' do
      get user_path(user)
      expect(response.body).to include user.username
    end
    it 'showアクションにユーザー画像が存在する' do
      get user_path(user)
      expect(response.body).to include 'test.jpg'
    end
    it 'showアクションに自己紹介が存在する' do
      get user_path(user)
      expect(response.body).to include user.profile
    end
  end
end
