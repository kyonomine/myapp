# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Product::request', type: :request do
  let!(:user) { create(:user) }
  let(:user_params) { attributes_for(:user) }
  let(:other_user) { create(:user, username: 'other_user') }
  let!(:product) { create(:product) }
  let(:product_params) { attributes_for(:product) }
  let(:invalid_product) { attributes_for(:product, product_name: '') }
  let(:update_product_params) { attributes_for(:product, product_name: 'ワイン') }

  describe 'GET #new' do
    before do
      sign_in user
      get new_product_path
    end
    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end
  end

  describe 'GET #edit' do
    context 'ログインしている場合' do
      before do
        sign_in user
      end
      it 'リクエストが成功すること' do
        get edit_product_path(product)
        expect(response.status).to eq 200
      end
      it 'アピール文が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.appeal
      end
      it 'ユーザー名が存在する' do
        get edit_product_path(product)
        expect(response.body).to include user.username
      end
      it '商品名が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.product_name
      end
      it '商品価格が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.product_price.to_s
      end
      it 'アルコール度数が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.product_alcohol.to_s
      end
      it '企業名が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.product_company
      end
      it '商品説明が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.product_introduction
      end
      it '商品評価が存在する' do
        get edit_product_path(product)
        expect(response.body).to include product.evaluation.to_s
      end
    end
    context 'as a guest' do
      it 'responds successfully' do
        sign_in other_user
        get edit_product_path(product)
        expect(response.status).to eq 200
      end
    end
  end

  describe 'GET #show' do
    before do
      sign_in user
    end
    it 'リクエストが成功すること' do
      get product_path(product)
      expect(response.status).to eq 200
    end
    it 'アピール文が存在する' do
      get product_path(product)
      expect(response.body).to include product.appeal
    end
    it 'ユーザー名が存在する' do
      get product_path(product)
      expect(response.body).to include user.username
    end
    it '商品名が存在する' do
      get product_path(product)
      expect(response.body).to include product.product_name
    end
    it '商品画像が存在する' do
      get product_path(product)
      expect(response.body).to include 'test.jpg'
    end
    it '商品価格が存在する' do
      get product_path(product)
      expect(response.body).to include product.product_price.to_s
    end
    it 'アルコール度数が存在する' do
      get product_path(product)
      expect(response.body).to include product.product_alcohol.to_s
    end
    it '企業名が存在する' do
      get product_path(product)
      expect(response.body).to include product.product_company
    end
    it '商品説明が存在する' do
      get product_path(product)
      expect(response.body).to include product.product_introduction
    end
    it '商品評価が存在する' do
      get product_path(product)
      expect(response.body).to include product.evaluation.to_s
    end
  end

  describe '#create' do
    before do
      sign_in user
    end
    context '有効なパラメータの場合' do
      it 'リクエストは302 リダイレクトとなること' do
        post products_path, params: { product: product_params }
        expect(response.status).to eq 302
      end
      it 'データベースに新しいユーザーが登録されること' do
        expect do
          post products_path, params: { product: product_params }
        end.to change(Product, :count).by(1)
      end
    end
    context '無効なパラメータの場合' do
      it 'リクエストは200となること' do
        post products_path, params: { product: invalid_product }
        expect(response.status).to eq 200
      end
      it 'データベースに新しいproductが登録されないこと' do
        expect do
          post products_path, params: { product: invalid_product }
        end.not_to change(Product, :count)
      end
    end
  end

  describe '#update' do
    before do
      sign_in user
    end
    context '正常な値の場合' do
      it '更新されること' do
        patch product_path(product), params: { product_id: product.id, product: update_product_params }
        expect(product.reload.product_name).to eq 'ワイン'
      end
      it 'ユーザーを更新した後の商品詳細画面にリダイレクト' do
        patch product_path(product), params: { product_id: product.id, product: update_product_params }
        expect(response).to redirect_to product_path(product.id)
      end
    end
    context '不正な値の場合' do
      it '更新されないこと' do
        patch product_path(product), params: { product_id: product.id, product: invalid_product }
        expect(product.reload.product_name).to eq 'ビール'
      end
    end
  end

  describe '#destroy' do
    before do
      sign_in user
    end
    context '商品削除' do
      it '商品を削除する' do
        expect do
          delete product_path(product), params: { id: product.id }
        end.to change(Product, :count).by(-1)
      end
      it '商品を削除した後のリダイレクト' do
        delete product_path(product), params: { product: product_params }
        expect(response).to redirect_to product_user_path(product.user_id)
      end
    end
  end
end
