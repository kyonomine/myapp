# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Diaries', type: :request do
  let!(:user) { create(:user) }
  let(:diary_params) { attributes_for(:diary) }
  let(:invalid_diary) { attributes_for(:diary, title: '') }
  let(:diary_detail_params) { attributes_for(:diary_detail) }
  let(:update_diary_params) { attributes_for(:diary, title: '昼飲み') }
  let!(:diary) { create(:diary, diary_details_attributes: [diary_detail_params]) }
  let(:diary_a) { create(:diary) }

  before do
    sign_in user
  end

  describe '#index' do
    it '正常なレスポンスか確認' do
      get diaries_path
      expect(response).to be_success
    end
    it 'レスポンスが200か確認' do
      get diaries_path
      expect(response).to have_http_status '200'
    end
  end

  describe 'GET #new' do
    before do
      get new_diary_path
    end
    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end
  end

  describe '#create' do
    before do
      sign_in user
    end
    context '有効なパラメータの場合' do
      it 'リクエストは302となること' do
        post diaries_path, params: { diary: diary_params }
        expect(response.status).to eq 302
      end
      it 'データベースに新しいユーザーが登録されること' do
        expect do
          post diaries_path, params: { diary: diary_params }
        end.to change(Diary, :count).by(1)
      end
    end
    context '無効なパラメータの場合' do
      it 'リクエストは200となること' do
        post diaries_path, params: { diary: invalid_diary }
        expect(response.status).to eq 200
      end
      it 'データベースに新しいproductが登録されないこと' do
        expect do
          post diaries_path, params: { diary: invalid_diary }
        end.not_to change(Diary, :count)
      end
    end
  end

  describe 'GET #edit' do
    context 'ログインしている場合' do
      before do
        sign_in user
        get edit_diary_path(diary)
      end
      it 'リクエストが成功すること' do
        expect(response.status).to eq 200
      end
      it 'タイトルが存在する' do
        expect(response.body).to include diary.title
      end
      it '日記商品名が存在する' do
        expect(response.body).to include diary_detail_params[:diary_product_name]
      end
      it '日記商品アルコールが存在する' do
        expect(response.body).to include diary_detail_params[:diary_product_alcohol].to_s
      end
      it '日記商品価格が存在する' do
        expect(response.body).to include diary_detail_params[:diary_product_price].to_s
      end
    end
  end

  describe 'GET #show' do
    before do
      get diary_path(diary)
    end
    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end
    it 'タイトルが存在する' do
      expect(response.body).to include diary.title
    end
    it '日記商品名が存在する' do
      expect(response.body).to include diary_detail_params[:diary_product_name]
    end
    it '日記商品アルコールが存在する' do
      expect(response.body).to include diary_detail_params[:diary_product_alcohol].to_s
    end
    it '日記商品価格が存在する' do
      expect(response.body).to include diary_detail_params[:diary_product_price].to_s
    end
  end

  describe '#update' do
    before do
      sign_in user
    end
    context '正常な値の場合' do
      it '更新されること' do
        patch diary_path(diary), params: { diary_id: diary.id, diary: update_diary_params }
        expect(diary.reload.title).to eq '昼飲み'
      end
      it 'ユーザーを更新した後の商品詳細画面にリダイレクト' do
        patch diary_path(diary), params: { diary_id: diary.id, diary: update_diary_params }
        expect(response).to redirect_to diary_path(diary.id)
      end
    end
    context '不正な値の場合' do
      it '更新されないこと' do
        patch diary_path(diary), params: { diary_id: diary.id, diary: invalid_diary }
        expect(diary.reload.title).to eq '宅飲み'
      end
    end
  end

  describe '#destroy' do
    before do
      sign_in user
    end
    context '削除' do
      it '日記を削除する' do
        expect do
          delete diary_path(diary), params: { id: diary.id }
        end.to change(Diary, :count).by(-1)
      end
      it '日記を削除した後のリダイレクト' do
        delete diary_path(diary), params: { dairy: diary_params }
        expect(response).to redirect_to diaries_path
      end
    end
  end
end
