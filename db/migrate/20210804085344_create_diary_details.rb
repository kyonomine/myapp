# frozen_string_literal: true

class CreateDiaryDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :diary_details do |t|
      t.integer :diary_id
      t.string :title
      t.text :impression
      t.datetime :start_time
      t.string :diary_product_name
      t.integer :diary_product_alcohol
      t.integer :diary_product_price
      t.timestamps
    end
  end
end
