# frozen_string_literal: true

class AddTotalToDiaryDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :diary_details, :total, :integer
  end
end
