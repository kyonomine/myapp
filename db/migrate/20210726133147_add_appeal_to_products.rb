# frozen_string_literal: true

class AddAppealToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :appeal, :string
  end
end
