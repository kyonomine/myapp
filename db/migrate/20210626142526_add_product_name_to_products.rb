# frozen_string_literal: true

class AddProductNameToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :product_name, :string
    add_column :products, :product_price, :integer
    add_column :products, :product_company, :string
    add_column :products, :product_image, :string
    add_column :products, :product_introduction, :string
    add_column :products, :product_alcohol, :integer
  end
end
