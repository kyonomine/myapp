# frozen_string_literal: true

class AddImpressionToDiaries < ActiveRecord::Migration[5.2]
  def change
    add_column :diaries, :impression, :text
  end
end
