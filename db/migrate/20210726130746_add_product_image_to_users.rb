# frozen_string_literal: true

class AddProductImageToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :product_image, :string
  end
end
