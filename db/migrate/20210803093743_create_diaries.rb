# frozen_string_literal: true

class CreateDiaries < ActiveRecord::Migration[5.2]
  def change
    create_table :diaries do |t|
      t.string :title
      t.text :content
      t.datetime :start_time
      t.string :product_name
      t.integer :product_alcohol
      t.integer :product_price

      t.timestamps
    end
  end
end
