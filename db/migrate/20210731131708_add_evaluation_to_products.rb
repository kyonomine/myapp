# frozen_string_literal: true

class AddEvaluationToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :evaluation, :float
  end
end
