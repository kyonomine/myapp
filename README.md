# 酒楽 ~ Liqour Enjoy ~

**"酒楽 ~ Liqour Enjoy ~"** is a service to enjoy alcohol more  
(**"酒楽 ~ Liqour Enjoy ~"** は、お酒をもっと楽しむサービスです)  

![image](https://bitbucket.org/kyonomine/myapp/raw/549d23e134ed091d1df1cecc79bc2a9f2adb499f/README_image/top_page.png)

# Why did you make it(作成した経緯)

It was difficult to enjoy alcohol because I had experience working at a railway company and was particularly strict about alcohol.  
(鉄道会社で働いた経験があり、特にお酒に厳しいので、お酒を楽しむのは大変でした。)

I like drinking alcohol and am interested in delicious and rare alcohol.  
(私はお酒を飲むのが好きで、美味しくて珍しいお酒に興味があります。)

I wanted to share the delicious sake and want other people to drink it, and I wanted to drink more delicious sake.  
(美味しいお酒をシェアして他の人にも飲んでもらいたいし、もっと美味しいお酒を飲みたいと思いました。)

I want more people to enjoy alcohol, such as those who like alcohol, those who are interested in alcohol, and those who like alcohol but do not know what to drink.  
(アルコールが好きな人、お酒に興味がある人、お酒は好きだけど何を飲めばいいのかわからない人など、もっとたくさんの人に飲酒を楽しんでもらいたいです。)

Therefore, I made it with the desire to have more people enjoy sake.  
(そこで、もっと多くの人にお酒を楽しんでもらいたいという思いで作りました。)

# Functions list(機能一覧)

* **User registration/ User login(ユーザーのアカウント作成・ログイン)**  
 You can create an account and log in  
 (アカウントを作成、ログインすることができます)

* **SEARCH(検索)**  
 Find the sake you care about  
 (気になるお酒を探せます)
 
* **DIARY(日記・記録)**  
 Keep a record of the alcohol you drank  
 (飲んだお酒を記録できます)
 
* **CHAT(チャット)**  
 Chat with like-minded users  
 (ユーザー同士でチャットできます)
 
* **POST(投稿)**  
 Share your favorite liquor  
 (好きなお酒を共有できます)
 
* **LIKE(いいね機能)**  
 Always check the liquor you care about  
 (気になるお酒を"いいね"して記録できます)
 
# How to use(使い方)
1 Register for an account by signing up in the header  
(ヘッダーのサインアップからアカウント登録をします)
If you have an account, you can log in from the login in the header  
(アカウントをお持ちの方はヘッダーのログインからログインできます)  
2 After logging in, you can move to each page of "Product Search", "Product Posting", "Drinking Friend (Other User) Search", and "Diary Posting" from the top page  
(ログイン後、トップページから『商品の検索』『商品の投稿』『飲み友(他のユーザー)の検索』『日記の投稿』各ページに移動できます)  
  * **Search for products (商品の検索)**  
    You can search for posted products from any word  
    (投稿されている商品を任意のワードから検索できます)  
  * **Posting a product (商品の投稿)**  
    You can post a product  
    (商品を投稿できます)  
  * **Search for drinking friends (飲み友の検索)**  
    you can search for other users, browse the details page, and chat  
    (他のユーザーを検索し詳細ページの閲覧、チャットができます)  
  * **Posting a diary (日記の投稿)**  
    You can record the alcohol you drank each day  
    (日ごとに飲んだお酒を記録できます)  

# DEMO
 
![image](https://bitbucket.org/kyonomine/myapp/raw/549d23e134ed091d1df1cecc79bc2a9f2adb499f/README_image/README_DEMO.gif)
 
# Technology used(使用技術)

* Ruby 2.7.2  
* Ruby on Rails 5.2.6  
* Mysql 5.7  
* AWS  
* Docker/Docker-compose  
* CircleCi CI/CD  
* Rspec  
  * Unit test(単体テスト): models  
  * Functional test(機能テスト): requests  
  * Integration test(統合テスト): system  
 
# ER図

![image](https://bitbucket.org/kyonomine/myapp/raw/b26f021d9eb39a3cd85a4b704693db11ba4e3f19/README_image/erd.png
)

# Features(特徴)
 
* 商品の評価を設定できるので、直感的に商品を捉えることができる

* 日記機能でお酒を飲んだ時間や商品、金額、コメントを残すことで思い出を残すことができます

* 他のユーザーが登録した数々の商品から、様々なワードで検索ができるので、好みの商品を探せます

* 飲み友を探せるのでお酒を通じて交友を深めることができます
 
# Author
 
NAME: Tomoya Kyonomine  
